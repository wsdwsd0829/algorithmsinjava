package arrays_and_strings;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AllUniqueCharacterInStringTest {
	AllUniqueCharacterInString auc;
	@Before
	public void setup(){
		auc = new AllUniqueCharacterInString();
	}
	@Test
	public void testUnique() {
		auc.str = "测试unicode ";
		assertTrue(auc.isAllUnique());
	}
	@Test
	public void testDup() {
		auc.str = "测试unicode 测";
		assertFalse(auc.isAllUnique());
	}

}
