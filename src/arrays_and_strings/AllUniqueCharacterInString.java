package arrays_and_strings;

import java.util.HashMap;
import java.util.Map;

public class AllUniqueCharacterInString {
	public String str;
	public AllUniqueCharacterInString() {
	}
	public AllUniqueCharacterInString(String s) {
		// TODO Auto-generated constructor stub
		str = s;
	}
	public boolean isAllUnique(){
		char[] charArr = str.toCharArray();
		Integer[] charMap = new Integer[65536];
		for(int i = 0; i < str.length(); i++) {
		    char c = str.charAt(i);
		    System.out.println(c); //this print out codepoint
		    // Process c...
		}
		Map<Character, Integer> map = new HashMap<Character , Integer>();
		for(char c : charArr){
			System.out.println(c);
			Character capitalC = Character.valueOf(c);
			if(map.containsKey(c) && map.get(capitalC) >=1){
				return false;
			}else{
				map.put(capitalC, map.containsKey(c)? map.get(capitalC) + 1: 1);
			}
		}
		return true;
	}
	public static void main(String[] args){
		System.out.println("cout");
		String s = "测试unicode ";
		AllUniqueCharacterInString auc = new AllUniqueCharacterInString(s);
		System.out.println(auc.isAllUnique());
	}
}
